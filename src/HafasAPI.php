<?php

namespace ThinkMobiles\HafasApiConnector;

/**
 * Class Hafas Api connector
 *
 * Represent wrapper for Hafas API
 *
 * @package ThinkMobiles\HafasAPI
 */

class HafasAPI
{

    /**
     * @var array
     *
     *  array with request options
     */

    private $_req = [];


    /**
     * @var array
     *
     *  array with response data
     */

    private $_res = [];

    /**
     *
     * Finds all routes for specified points
     *
     * @param string $start
     * @param string $destination
     * @param array $options
     *
     * @return array
     *
     * @throws NotFoundRouteException
     *
     */

    public function findRoutes(string $start, string $destination, $options = [])
    {

        $startLocation  = $this->findLocation( $start )[0];
        $destLocation   = $this->findLocation( $destination )[0];
        $res            = null;
        $routes         = null;
        $departureTime  = $options['departureTime'] ?? null;

        $this->_req['body'] = RequestConstructor::connectionRequest( $startLocation, $destLocation, $departureTime );

        $this->makeRequest();

        $res = $this->_res;

        $this->clear();

        $routes = ( new ResponseParser( $res ) )->connectionResponseParse();

        if( !$routes ) throw new NotFoundRouteException( $start, $destination );

        return [
            'start'  => $startLocation,
            'dest'   => $destLocation,
            'routes' => $routes
        ];

    }

    /**
     *
     * Finds location by specified name
     *
     * @param string $name
     * @param array $options
     *
     * @return array
     *
     * @throws NotFoundLocationException
     */

    public function findLocation(string $name, array $options = [])
    {

        $locations = null;
        $res = null;

        $this->_req['body'] = RequestConstructor::locationValidationRequest($name, $options);
        $this->makeRequest();

        $res = $this->_res;

        $this->clear();

        $locations = ( new ResponseParser($res) )->locationResponseParse();

        if( !$locations ) throw new NotFoundLocationException($name);

        return $locations;
    }


    /**
     *
     * makes request to hafas system and retrieve response
     *
     * @param bool $raw
     */

    private function makeRequest(bool $raw = false)
    {

        $ch = null;

        $this->beforeRequest();
        $ch = curl_init();
        curl_setopt_array($ch,
            [
                CURLOPT_URL => config('hafas.baseUrl'),
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_POST => 1,
                CURLOPT_TIMEOUT => 10,
                CURLOPT_POSTFIELDS => $this->_req['body'],
                CURLOPT_HTTPHEADER => ['Connection: close', 'Content-Type: text/xml']
            ]
        );

        $this->_res = curl_exec($ch);

        if(!$raw) $this->afterRequest();

    }

    /**
     *
     *  prepares data to request
     *
     */

    private function beforeRequest()
    {
    }


    /**
     *
     * processes given data after request
     *
     */

    private function afterRequest()
    {
        file_put_contents(base_path('test.xml'), $this->_res);
    }


    /**
     * clears request and response arrays
     */

    private function clear()
    {
        $this->_req = [];
        $this->_res = [];
    }

    public function __toString()
    {
        return "HAFAS API";
    }

}
