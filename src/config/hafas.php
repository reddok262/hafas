<?php

return [
    'accessId'    => '',
    'baseUrl'     => 'http://fahrplan.oebb.at/bin/query.exe',
    'route'       => [
        'results'    => 5,
        'maxTransfers' => 10,
        'transferTime' => 0
    ],
    'version'     => '1.1',
    'timezone'    => 'Europe/Berlin',
    'lang'        => 'en',
    'location'    => [
        'refine'     => true,
        'results'   => 5,
        'stations'  => true,
        'addresses' => true,
        'poi'       => true
    ]
];