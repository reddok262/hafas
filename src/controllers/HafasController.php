<?php

namespace ThinkMobiles\HafasApiConnector;

use App\Http\Controllers\Controller;

class HafasController extends Controller
{

    function getRoute () {

        $locations= \Request::only('start', 'dest');

        return \HafasAPI::findRoutes($locations['start'], $locations['dest']);
    }

    function getLocation () {

        $name = \Request::get('name');
        return \HafasAPI::findLocation($name);

    }

}