<?php

namespace ThinkMobiles\HafasApiConnector;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/hafas.php' => config_path('hafas.php')
        ]);

        $this->loadRoutesFrom(__DIR__ . '/routes/api.php');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

        $this->loadMigrationsFrom(__DIR__.'/migrations');

        $this->app->bind('HafasApiConnector', HafasAPI::class);

        $this->mergeConfigFrom(
            __DIR__. '/config/hafas.php', 'hafas'
        );
    }
}
