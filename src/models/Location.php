<?php
namespace ThinkMobiles\HafasApiConnector;

class Location implements  EntityInterface
{

    static private $types = [
        'Station', 'Address', 'Poi'
    ];

    static public function build(array $data, string $type = 'Station')
    {
        $data = $data['@attributes'] ?? $data;
        $result = [];
        $result['type'] = strtolower($type);

        if ( in_array($type, ['Poi', 'Station']) ) {
            $result['id'] = isset($data['externalStationNr'])? preg_replace('/^0+/', '', $data['externalStationNr'] ) : null;
        } else {
            $result['id'] = null;
        }

        $result['name'] = $data['name'];

        $result['coordinates'] = [
            'lat' => CoordHelper::parse( $data['y'] ) ,
            'lon' => CoordHelper::parse( $data['x'] )
        ];

        return $result;
    }

    static public function getTypes() {
        return static::$types;
    }
}