<?php

namespace ThinkMobiles\HafasApiConnector;


class Step implements EntityInterface
{

    static private $types = [
        'Walk',
        'Journey',
        'GisRoute',
        'Transfer'
    ];

    static private $_date = '';

    static public function setDate($date) {
        static::$_date = $date;
    }

    static public function build(array $response)
    {

        $result = [];

        $result['start'] = Location::build(
            $response['Departure']['BasicStop']['Address']['@attributes']
            ??
                 $response['Departure']['BasicStop']['Station']['@attributes']
        );

        $result['dest'] = Location::build(
            $response['Arrival']['BasicStop']['Address']['@attributes']
            ??
                  $response['Arrival']['BasicStop']['Station']['@attributes']
        );

        $result['departure'] = DateHelper::getDateOfLocation( static::$_date, $response['Departure']['BasicStop']['Dep']['Time'] ?? '' );
        $result['arrival']   = DateHelper::getDateOfLocation( static::$_date, $response['Arrival']['BasicStop']['Arr']['Time'] ?? '' );
        $result['duration']  = DateHelper::getInMinutes( DateHelper::findDifferenceInTime( $result['departure'], $result['arrival'] ) );

        $crossedTypes =  array_intersect_key( $response, array_flip( self::$types ));

        if( $crossedTypes ) {
            $fn = 'type' . array_keys($crossedTypes)[0];
            static::$fn($response, $result);
        }

        return $result;
    }

    static private function typeWalk($data, &$result) {
        $result['mode'] = 'walk';
        $result['length'] = $data['Walk']['@attributes']['length'];
    }

    static private function typeTransfer($data, &$result) {
        $result['mode'] = 'walk';
        $result['length'] = $data['Transfer']['@attributes']['length'];
    }

    static private function typeJourney($data, &$result) {
        $result['mode'] = 'journey';
        $result['length'] = $data['Journey']['@attributes']['length'];
    }

    static private function typeGisRoute($data, &$result) {

        if( $data['GisRoute']['@attributes']['type'] === 'FOOT' ) {
            $result['mode'] = 'walk';
        } else {
            $result['mode'] = strtolower( $data['GisRoute']['@attributes']['type'] );
        }

        $result['length'] = $data['GisRoute']['Distance'];
    }

}
