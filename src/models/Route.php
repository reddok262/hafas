<?php


namespace ThinkMobiles\HafasApiConnector;

class Route implements EntityInterface
{

    static public function build(array $response)
    {

        $result = [];

        $container = $response['ConSectionList'];

        if( !isset($container['ConSection']) ) {
            $container = [];
        } else {
            $container = ArrayHelper::is_indexed($container['ConSection'])? $container['ConSection'] : [ $container['ConSection'] ];
        }

        Step::setDate($response['Overview']['Date']);

        $steps = array_map([Step::class, 'build'], $container);
        $countSteps = count($steps);

        $result['departure'] =  $steps[0]['departure'] ?? null;
        $result['arrival'] = $steps[$countSteps - 1]['arrival'] ?? null;

        if( isset( $response['Overview']['Duration']['Time'] ) ) {
            $time = DateHelper::createDateFromHafas( $response['Overview']['Date'], $response['Overview']['Duration']['Time'] );

            $result['duration'] = DateHelper::findDifferenceInTime( $time );
            $result['duration'] = DateHelper::getInMinutes( $result['duration'] );
        } else {
            $result['duration'] = array_sum( array_column($steps, 'duration') ) . 'min';
        }

        $result['steps'] = $steps;

        return $result;

    }
}