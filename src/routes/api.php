<?php

namespace ThinkMobiles\HafasApiConnector;


        /**
         * @SWG\Post(
         *     path="/hafas/route",
         *     tags={"HAFAS"},
         *     description="Find route between two locations",
         *     produces= {"application/json"},
         *
         *     @SWG\Parameter(
         *         name="",
         *         in="body",
         *         description="",
         *         required=true,
         *         @SWG\Schema(
         *             ref="#/definitions/hafas-route",
         *         )
         *     ),
         *
         *     @SWG\Response(
         *         response="200",
         *         description="Found routes"
         *     ),
         *     @SWG\Response(
         *         response="403",
         *         description="Access forbidden"
         *     ),
         *     @SWG\Response(
         *         response="default",
         *         description="Unexpected error",
         *         @SWG\Schema(
         *             ref="#/definitions/error"
         *         )
         *     )
         * )
         */


        \Route::get('/hafas/route', HafasController::class . '@getRoute');


        /**
         * @SWG\Post(
         *     path="/hafas/location",
         *     tags={"HAFAS"},
         *     description="Find location parameters by given name",
         *     produces= {"application/json"},
         *
         *     @SWG\Parameter(
         *         name="",
         *         in="body",
         *         description="",
         *         required=true,
         *         @SWG\Schema(
         *             ref="#/definitions/hafas-location",
         *         )
         *     ),
         *
         *     @SWG\Response(
         *         response="200",
         *         description="Found location"
         *     ),
         *     @SWG\Response(
         *         response="403",
         *         description="Access forbidden"
         *     ),
         *     @SWG\Response(
         *         response="default",
         *         description="Unexpected error",
         *         @SWG\Schema(
         *             ref="#/definitions/error"
         *         )
         *     )
         * )
         */

        \Route::get('/hafas/location', HafasController::class . '@getLocation');


