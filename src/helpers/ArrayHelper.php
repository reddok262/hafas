<?php
namespace ThinkMobiles\HafasApiConnector;

/**
 * Class ArrayHelper
 *
 * helper functions for arrays
 *
 * @package ThinkMobiles\HafasAPI
 */

class ArrayHelper
{

    /**
     * checks if array indexed
     *
     * @param array $arr
     *
     * @return bool
     */

    static public function is_indexed(array $arr)
    {

        foreach ($arr as $key => $value) {
            if (is_string($key)) return false;
        }

        return true;

    }

}