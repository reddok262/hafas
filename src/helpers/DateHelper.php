<?php

namespace ThinkMobiles\HafasApiConnector;

/**
 * Class DateHelper
 *
 * class consists functions that help handle dates
 *
 * @package ThinkMobiles\HafasAPI
 */

class DateHelper
{

    /**
     * default date format for wrapper
     *
     * @var string
     */

    static private $defaultFormat = 'Y/m/d H:i:s';

    /**
     * changes default date format
     *
     * @param string $format
     */

    static public function setDefaultFormat(string $format) {
        static::$defaultFormat = $format;
    }

    /**
     * calculates difference between times
     *
     * @param $start
     * @param $end
     *
     * @return int
     */

    static public function findDifferenceInTime($start, $end = null) {

        if( is_string($start) ) $start = static::createDate($start);

        if($end) {
            if( is_string($end) ) $end = static::createDate($end);
        } else {
            $end = clone($start);
            $start->setTime(0,0,0);
        }

        return $end->getTimestamp() - $start->getTimestamp();

    }

    /**
     * format date for location
     *
     * @param string $dateString
     * @param string $timeString
     *
     * @return string
     */

    public static function getDateOfLocation(string $dateString, string $timeString) {

        $date = null;
        $result = null;

        if(!$timeString) $timeString = ( new \DateTime() )->format('H:i:s');

        $date = static::createDateFromHafas( $dateString, $timeString );

        return $date->format( static::$defaultFormat );

    }

    /**
     * creates date from specified string with ceratin format
     *
     * @param string|null $string
     * @param string|null $format
     *
     * @return bool|\DateTime
     */

    public static function createDate(string $string = null, string $format = null) {

        if(!$string) return new \DateTime( 'now', new \DateTimeZone( config('hafas.timezone' ) ) );
        if(!$format) $format = static::$defaultFormat;

        return \DateTime::createFromFormat($format, $string , new \DateTimeZone( config('hafas.timezone') ) );
    }

    /**
     * extract time from hafas string
     *
     * @param $string
     *
     * @return bool|string
     */

    public static function extractTime(string $string) {
        return substr($string,-8);
    }

    public static function extractPassedDays(string $string) {
        return substr($string, 0, 2);
    }
    /**
     * return date in minutes
     *
     * @param $timestamp
     *
     * @return string
     */

    public static function getInMinutes(int $timestamp) {
        return ( (int) ($timestamp / 60) ) . 'min';
    }

    static public function createDateFromHafas(string $dateString, string $timeString) {

        $passedDays = null;
        $date = null;
        $time = DateHelper::extractTime($timeString);

        if( strlen( $timeString ) > 8 )  $passedDays = (int) DateHelper::extractPassedDays($timeString);

        $date = DateHelper::createDate( $dateString . ' ' . $time, 'Ymd H:i:s' );
        if($passedDays) $date->modify( '+' . $passedDays . ' day' );

        return $date;
    }
}