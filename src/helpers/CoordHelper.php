<?php

namespace ThinkMobiles\HafasApiConnector;

/**
 * Class CoordHelper
 *
 *  helper that handle transformations of $coordinates for properly work with Hafas
 *
 * @package ThinkMobiles\HafasAPI
 */

class CoordHelper
{
    /**
     *
     *
     * @var int
     */

    static private $_modifier = 1000000;

    /**
     * transforms coordinate in hafas format
     *
     * @param $coord
     *
     * @return mixed
     */

    static public function stringify(float $coord)
    {
        return $coord * static::$_modifier;
    }

    /**
     * transforms coordinate from hafas format
     *
     * @param $coord
     * @return float|int
     */

    static public function parse(float $coord)
    {
        return $coord / static::$_modifier;
    }

}