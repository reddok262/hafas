<?php


namespace ThinkMobiles\HafasApiConnector;

/**
 * Class TransportHelper
 *
 * helper for find public transport
 *
 * @package ThinkMobiles\HafasAPI
 */

class TransportHelper {

    /**
     * relations between hafas class and public transport. But public transport for Germany...
     *
     * @var array
     */

    private static $transportBitmask = [
        1 => [
            'name'    => 'InterCityExpress',
            'short'   => 'ICE',
            'mode'    => 'train',
            'product' => 'nationalExp'
        ],
        2 => [
            'name'    => 'InterCity & EuroCity',
            'short'   => 'IC/EC',
            'mode'    => 'train',
            'product' => 'national',
        ],
        4 => [
            'name'    => 'InterRegio',
            'short'   => 'IR',
            'mode'    => 'train',
            'product' => 'regionalExp'
        ],
        8 => [
            'name'    => 'RegionalExpress & Regio',
            'short'   => 'RE/RB',
            'mode'    => 'train',
            'product' => 'regional'
        ],
        16 => [
            'name'    => 'S-Bahn',
            'short'   => 'S',
            'mode'    => 'train',
            'product' => 'suburban'
        ],
        32 => [
            'name'    => 'Bus',
            'short'   => 'B',
            'mode'    => 'bus',
            'product' => 'bus'
        ],
        64 => [
            'name'    => 'Ferry',
            'short'   => 'F',
            'mode'    => 'ferry',
            'product' => 'ferry'
        ],
        128 => [
            'name'    => 'U-Bahn',
            'short'   => 'U',
            'mode'    => 'train',
            'product' => 'subway'
        ],
        256 => [
            'name'    => 'Tram',
            'short'   => 'T',
            'mode'    => 'tram',
            'product' => 'tram'
        ],
        512 => [
            'name'    => 'Group Taxi',
            'short'   => 'Taxi',
            'mode'    => null,
            'product' => 'taxi'
        ],
        0 => [
            'name'    => 'unknown',
            'short'   => '?',
            'product' => 'unknown'
        ]
    ];

    /**
     * returns transport data by given class
     *
     * @param int $class
     *
     * @return mixed
     */

    static public function getTransportByClass(int $class) {

        return static::$transportBitmask[$class];

    }

}