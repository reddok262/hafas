<?php

namespace ThinkMobiles\HafasApiConnector;


class RequestConstructor {

    static $methods = [
        'locationValidationRequest',
        'connectionRequest'
    ];

    private static function _locationValidationRequest($stationName, $options)
    {
        $xml = '<LocValReq id="PROD Filtering" maxNr="' . config('hafas.location.results') .'">
                        <LocFilter ftype="PROD" />
                        <ReqLoc match="'.$stationName.'" type="ALLTYPE" refine="true"/>
                    </LocValReq>';

        return $xml;
    }

    private static function _connectionRequest(array $start, array $dest, $departureTime)
    {

        return '<ConReq ivCons="yes" oevCons="yes">
                        <Start>
                            <Coord x="'. CoordHelper::stringify( $start['coordinates']['lon'] ) .'" y="'. CoordHelper::stringify( $start['coordinates']['lat'] ) .'" type="WGS84" />
                            <Prod prod="1111111111111111" bike="0" couchette="0" direct="0" sleeper="0" />
                        </Start>
                        <Dest>
                            <Coord x="'. CoordHelper::stringify( $dest['coordinates']['lon'] ) .'" y="'. CoordHelper::stringify( $dest['coordinates']['lat'] ) .'" type="WGS84" />
                        </Dest>
                        <ReqT time="21:40" date="20170805" />
                        <RFlags b="0" f="' . config('hafas.route.results') . '" chExtension="0" sMode="N" getPrice="1" />
                    </ConReq>';

    }

    static public function __callStatic($method, $args) {

        if( !in_array($method, static::$methods) ) return false;
        $method = '_' . $method;

        $xml = '<?xml version="1.0" encoding="iso-8859-1"?>
                <ReqC ver="' . config('hafas.version') . '" prod="String" lang="' . config('hafas.lang') . '" accessId="'. config('hafas.accessId') .'">';
        $xml .= static::$method(...$args);
        $xml .= '</ReqC>';

        return $xml;

    }
}