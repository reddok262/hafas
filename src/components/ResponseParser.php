<?php

namespace ThinkMobiles\HafasApiConnector;

class ResponseParser {

    private $_response = [];

    public function __construct($xml) {

        $this->_response = json_decode(json_encode( new \SimpleXMLElement($xml) ), true);
    }

    public function locationResponseParse()
    {

        $container = $this->_response['LocValRes'];
        $types = Location::getTypes();
        $locations = [];

        foreach($types as $type) {

            if( isset( $container[$type] ) ) {
                $data = ArrayHelper::is_indexed($container[$type])? $container[$type] : [$container[$type]];

                $locations = array_merge($locations, array_map(function($location) use ($type) {
                    return Location::build($location, $type);
                }, $data));
            }

        }

        return $locations;

    }

    public function connectionResponseParse()
    {

        if( isset($this->_response['ConRes']['Err']) ) {
            if( $this->_response['ConRes']['Err']['@attributes']['code'] === 'K890' ) return [];
            throw new \Exception( $this->_response['ConRes']['Err']['@attributes']['text'] );
        }

        $container = $this->_response['ConRes']['ConnectionList'];

        if( !isset($container['Connection']) ) {
            $container = [];
        } else {
            $container = ArrayHelper::is_indexed($container['Connection'])? $container['Connection'] : [ $container['Connection'] ];
        }

        return array_map( [Route::class, 'build'], $container );

    }
}