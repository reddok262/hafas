<?php

namespace ThinkMobiles\HafasApiConnector;


interface EntityInterface {

    static public function build(array $data);

}