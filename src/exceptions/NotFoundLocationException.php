<?php


namespace ThinkMobiles\HafasApiConnector;

/**
 * Class NotFoundLocationException
 *
 * class that represent error when any location not found
 *
 * @package ThinkMobiles\HafasAPI
 */

class NotFoundLocationException extends \Exception
{

    /**
     * name of desired location
     *
     * @var string
     */

    private $_name;

    public function __construct( string $locationName )
    {
        parent::__construct('Location ' . $locationName . ' not found!');
        $this->_name = $locationName;
    }

    /**
     *
     * returns name of desired location
     *
     * @return string
     */

    public function getName()
    {
        return $this->_name;
    }

}