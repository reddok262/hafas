<?php


namespace ThinkMobiles\HafasApiConnector;

/**
 * Class NotFoundRouteException
 *
 * class that represents error when any route not found
 *
 * @package ThinkMobiles\HafasAPI
 */

class NotFoundRouteException extends \Exception
{

    /**
     * start location
     *
     * @var string
     */

    private $_start;

    /**
     * location of destination
     *
     * @var string
     */

    private $_dest;

    public function __construct( string $start, string $dest )
    {
        parent::__construct('Any routes between ' . $start . '  and ' . $dest . ' not found!');
        $this->_start = $start;
        $this->_dest = $dest;
    }


    /**
     * returns locations
     *
     * @return array
     */

    public function getLocations()
    {
        return [
            'start' => $this->_start,
            'dest'  => $this->_dest
        ];
    }


}