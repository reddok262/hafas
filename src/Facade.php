<?php
    namespace ThinkMobiles\HafasApiConnector;

    class Facade extends \Illuminate\Support\Facades\Facade
    {
        protected static function getFacadeAccessor()
        {
            return 'HafasApiConnector';
        }
    }